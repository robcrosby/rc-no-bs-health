import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:health_notes/workout.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  
  DateFormat weekday = new DateFormat("EEEE");
  DateFormat time = new DateFormat("h:mm a");
  DateTime today = new DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  
  String slug;
  TextEditingController slugController = new TextEditingController();

  Future<String> getSlug() async {
    if (slug == null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      slug = prefs.getString("slug");
    }
    return slug;
  }

  void setSlug(String _slug) async {
    Firestore.instance.collection("slug").document(_slug).setData({
      "created"  : DateTime.now().millisecondsSinceEpoch
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("slug", _slug);
    slug = _slug;

  }

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    print(widget.today);
      return 
      Scaffold(
        body: Container(
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            FutureBuilder(
              future: widget.getSlug(),
              builder: (_, AsyncSnapshot<String> slug) {
                if (slug.connectionState == ConnectionState.waiting) {
                  return Container(
                    padding: EdgeInsets.all(60),
                    child: Center(
                      child: CircularProgressIndicator()),
                  );
                } else if (slug.data != null) {
                  // slug found
                  return Container(
                    padding: EdgeInsets.all(30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          ("It's " + widget.weekday.format(widget.today)).toUpperCase(),
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Divider(),
                        StreamBuilder(
                          stream: Firestore.instance.collection("slug").document(slug.data).collection("days").document(widget.today.millisecondsSinceEpoch.toString()).snapshots(),
                          builder: (_, AsyncSnapshot<DocumentSnapshot> todaysWorkout) {
                            if (todaysWorkout.hasData) {
                              if (todaysWorkout.data.exists) {
                                return today(slug.data, todaysWorkout.data);
                              } else {
                                createToday(slug.data);
                              }
                            }
                            return Container(
                              padding: EdgeInsets.all(60),
                              child: Center(
                                child: CircularProgressIndicator()),
                            );
                          },
                        )
                      ],
                    )
                  );
                } else {
                  return Container(
                    padding: EdgeInsets.all(30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Get Started",
                          style: Theme.of(context).textTheme.headline,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: Text(
                            "Give yourself a name so you can track your progress over time.",
                            style: Theme.of(context).textTheme.body1,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: Material(
                            child: TextField(
                              controller: widget.slugController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(15),
                                hintText: "bob"
                              ),
                              onSubmitted: (result) {
                                widget.setSlug(result);
                                setState(() {
                                  
                                });
                              },
                            )
                          )
                        )
                      ],
                    ),
                  );
                }
                
                
              },
            )
          ],
        ),
      )
    );
  }

  void createToday(String slug) async {
    await Firestore.instance.collection("slug").document(slug).collection("days").document(widget.today.millisecondsSinceEpoch.toString()).setData({
      "date" : widget.today.millisecondsSinceEpoch
    });
    setState(() {
      
    });
  }

  List<String> workouts;


  Widget today(String slug, DocumentSnapshot today) {
    Firestore.instance.collection("slug").document(widget.slug).get().then((data) {
      if (workouts != data.data.keys.toList()) {
        workouts = data.data.keys.toList();
      }
    });
    List<Widget> workout = new List<Widget>();
    for (dynamic key in today.data.keys) {
      if (key != "date") {
        workout.add(
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    today.reference.updateData({
                      key : FieldValue.delete()
                    });
                  },
                ),
                Text(key,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold
                  )
                ),
                Spacer(),
                Text(today.data[key].toString(),
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold
                  )
                ),
                IconButton(
                  icon: Icon(Icons.add_box),
                  onPressed: () {
                    today.reference.updateData({
                      key : FieldValue.increment(1)
                    });
                  },
                ),
              ],
            ),
          )
        );
      }
    }
    if (workout.isEmpty) {
      workout.add(
        Text(
        "YOU HAVEN'T DONE ANYTHING YET",
        style: TextStyle(
          color: Colors.grey[400],
          fontSize: 24,
          fontWeight: FontWeight.bold
        )
      ));
    }
    workout.add(Divider());
    workout.add(
      NewWorkout(slug, today, workouts)
    );
    workout.add(
      Text(
        "WHEN YOU ADD A WORKOUT IT WILL BE SAVED, PREVIOUS WORKOUTS WILL AUTOFILL FOR QUICKLY ADDING. TAP ON A WORKOUT TO SEE YOUR PROGRESS OVER TIME",
        style: TextStyle(
          color: Colors.grey[400],
          fontSize: 16,
          fontWeight: FontWeight.bold
        )
      ));
    
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: workout
      )
    );
  }
}