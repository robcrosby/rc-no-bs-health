import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NewWorkout extends StatefulWidget {
  

  String current = "ADD WORKOUT";
  final List<String> workouts;
  final slug;
  final DocumentSnapshot today;

  NewWorkout(this.slug, this.today, this.workouts);

  @override
  _NewWorkoutState createState() => _NewWorkoutState();
}

class _NewWorkoutState extends State<NewWorkout> {


  TextEditingController controller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            child: Text(
              widget.current,
              style: TextStyle(
                color: Colors.grey[500],
                fontSize: 24,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          Positioned(
            child: Material(
              
              color: Colors.transparent,
              child: TextField(
                
                textCapitalization: TextCapitalization.characters,
                autocorrect: false,
                onChanged: (String value) {
                  setState(() {
                    if (value.isEmpty) {
                      widget.current = "ADD WORKOUT";
                    } else {
                      widget.current = autocomplete(value);
                    }
                    
                  });
                },
                onSubmitted: (String submission) {
                  if (submission.isNotEmpty) {
                    if (widget.current.isNotEmpty) {
                    controller.text = widget.current;
                    }
                    addWorkout(widget.current.isEmpty);
                  }
                },
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold
                ),
                controller: controller,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  border: InputBorder.none,
                  fillColor: Colors.white,
                  hintText: widget.current
                ),
              )
            ),
          )
        ]
      )
    );
  }

  String autocomplete(String input) {
    for (String word in widget.workouts) {
      if (word.contains(input, 0)) {
        return word;
      }
    }
    return "";
  }

  void addWorkout(bool create) async {
    if (create) {
      await createWorkout();
    }
    await widget.today.reference.updateData({
      controller.text : 0
    });
    controller.text = "";
  }

  Future<void> createWorkout() async {
    return Firestore.instance.collection("slug").document(widget.slug).updateData({
      controller.text : null
    });
  }
}